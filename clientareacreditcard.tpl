{php}
  if ($_REQUEST['error'] == 1) {
    $this->_tpl_vars['errormessage'] = "Your card could not be saved, please try again or contact support.";
   }
{/php}
{include file="$template/pageheader.tpl" title=$LANG.clientareanavccdetails}

{include file="$template/clientareadetailslinks.tpl"}

{if $remoteupdatecode}

  <div align="center">
    {$remoteupdatecode}
  </div>

{else}

{if $successful}
<div class="alert alert-success">
    <p>{$LANG.changessavedsuccessfully}</p>
</div>
{/if}

{if $errormessage}
<div class="alert alert-error">
    <p class="bold">{$LANG.clientareaerrors}</p>
    <ul>
        {$errormessage}
    </ul>
</div>
{/if}
{literal}
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
            // this identifies your website in the createToken call below
            {/literal}
            {php}
              $result = select_query("tblpaymentgateways","value", array("gateway" => "stripe","setting" => "publishableKey"));
              $gateway_data = mysql_fetch_array($result);
              $this->_tpl_vars['publishableKey'] = $gateway_data['value'];
            {/php}
            Stripe.setPublishableKey('{$publishableKey}');
            var $customer_name = '{$clientsdetails.firstname|escape:'quotes'} {$clientsdetails.lastname|escape:'quotes'}';
            var $address_1 = '{$clientsdetails.address1|escape:'quotes'}';
            var $address_2 = '{$clientsdetails.address2|escape:'quotes'}';
            var $city = '{$clientsdetails.city|escape:'quotes'}';
            var $zip = '{$clientsdetails.postcode}';
            var $county = '{$clientsdetails.country}';
            var $state = '{$clientsdetails.state|escape:'quotes'}';
            
            var $continue_button = '{$LANG.clientareasavechanges|escape:'quotes'}';
            var $wait_button = '{$LANG.pleasewait|escape:'quotes'}';
            
            {literal}
            function stripeResponseHandler(status, response) {
                if (response.error) {
                    // re-enable the submit button
                    jQuery('.submit-button').removeAttr("disabled");
                    jQuery('.submit-button').attr("value",$continue_button);
                    // show the errors on the form
                    jQuery(".payment-errors").html(response.error.message);
                    jQuery(".payment-errors").show();
                } else {
                    var form$ = jQuery("#payment-form");
                    // token contains id, last4, and card type
                    var token = response['id'];
                     // insert the token into the form so it gets submitted to the server
                    form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                    // and submit
                    form$.get(0).submit();
                }
            }

            jQuery(document).ready(function() {
                jQuery("#payment-form").submit(function(event) {
                    // disable the submit button to prevent repeated clicks
                    jQuery('.submit-button').attr("disabled", "disabled");
                    jQuery('.submit-button').attr("value",$wait_button);

                    // createToken returns immediately - the supplied callback submits the form if there are no errors
                    Stripe.createToken({
                        number: jQuery('.card-number').val(),
                        cvc: jQuery('.card-cvc').val(),
                        exp_month: jQuery('.card-expiry-month').val(),
                        exp_year: jQuery('.card-expiry-year').val(),
                        name: $customer_name,
                        address_line1: $address_1,
                        address_line2: $address_2,
                        address_city: $city,
                        address_state: $state,
                        address_zip:  $zip,
                        address_country: $county,
                    }, stripeResponseHandler);
                    return false; // submit from callback
                });
            });
        </script>
 {/literal}
<div class="alert alert-error payment-errors" style="display:none;"></div>       
<form class="form-horizontal" method="post" id="payment-form" action="modules/gateways/stripe-php/stripesave.php">

  <fieldset class="onecol">

    <div class="control-group">
	    <label class="control-label">{$LANG.creditcardcardtype}</label>
		<div class="controls">
		    <input type="text" value="{$cardtype}" disabled="true" />
		</div>
	</div>

    <div class="control-group">
	    <label class="control-label">{$LANG.creditcardcardnumber}</label>
		<div class="controls">
		    <input type="text" value="{$cardnum}" disabled="true" />
		</div>
	</div>

    <div class="control-group">
	    <label class="control-label">{$LANG.creditcardcardexpires}</label>
		<div class="controls">
		    <input type="text" value="{$cardexp}" disabled="true" class="input-small" />
		</div>
	</div>
{if $cardissuenum}
    <div class="control-group">
	    <label class="control-label">{$LANG.creditcardcardissuenum}</label>
		<div class="controls">
		    <input type="text" value="{$cardissuenum}" disabled="true" class="input-small" />
		</div>
	</div>
{/if}{if $cardstart}
    <div class="control-group">
	    <label class="control-label">{$LANG.creditcardcardstart}</label>
		<div class="controls">
		    <input type="text" value="{$cardstart}" disabled="true" class="input-small" />
		</div>
	</div>
{/if}
{if $allowcustomerdelete && $cardtype}
    <div class="control-group">
	    <label class="control-label">&nbsp;</label>
		<div class="controls">
            <input class="btn btn-danger" type="button" value="{$LANG.creditcarddelete}" onclick="window.location='clientarea.php?action=creditcard&delete=true'" />
        </div>
    </div>
{/if}
  </fieldset>

<div class="styled_title"><h3>{$LANG.creditcardenternewcard}</h3></div>

  <br />

  <fieldset class="onecol">

    <div class="control-group">
	    <label class="control-label" for="ccnumber">{$LANG.creditcardcardnumber}</label>
		<div class="controls">
		    <input type="text" autocomplete="off" class="card-number" />
		</div>
	</div>

    <div class="control-group">
	    <label class="control-label" for="ccexpirymonth">{$LANG.creditcardcardexpires}</label>
		<div class="controls">
		    <select name="card-exp-month" class="card-expiry-month">{foreach from=$months item=month}<option>{$month}</option>{/foreach}</select> / <select  name="card-exp-year" class="card-expiry-year">{foreach from=$years item=year}<option>{$year}</option>{/foreach}</select>
		</div>
	</div>
	
    <div class="control-group">
       <label class="control-label" for="cccvv">{$LANG.creditcardcvvnumber}</label>
       <div class="controls"><input type="text" size="5" autocomplete="off" class="input-small newccinfo card-cvc" />&nbsp;<a href="#" onclick="window.open('images/ccv.gif','','width=280,height=200,scrollbars=no,top=100,left=100');return false">{$LANG.creditcardcvvwhere}</a></div>
    </div>

  </fieldset>

  <div class="form-actions">
    <input class="btn btn-primary submit-button" type="submit" value="{$LANG.clientareasavechanges}" />
    <input class="btn" type="reset" value="{$LANG.cancel}" />
  </div>

</form>

{/if}